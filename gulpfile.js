const
  // directory locations
  dir = {
    src: 'src/',
    build: 'build/'
  },
  // modules
  gulp = require('gulp'),
  newer = require('gulp-newer'),
  size = require('gulp-size'),
  imagemin = require('gulp-imagemin'),
  sass = require('gulp-sass'),
  postcss = require('gulp-postcss'),
  htmlmin = require('gulp-htmlmin'),
  fileinclude = require('gulp-file-include'),
  concat = require('gulp-concat'),
  terser = require('gulp-terser')

/**************** html task ****************/
const phpConfig = {
  src: dir.src + 'admin/**/*',
  build: dir.build + 'admin',
}

function php () {
  return gulp.src(phpConfig.src).pipe(gulp.dest(phpConfig.build))
}
exports.php = php

/**************** html task ****************/
const htmlConfig = {
  src: dir.src + 'views/*.html',
  build: dir.build,
  paths: [
    dir.src + 'views/*.html',
    dir.src + 'views/**/*.html',
    dir.src + 'views/**/**/*.html'
  ]
}

function html () {
  return gulp.src(htmlConfig.src)
    .pipe(fileinclude({
      prefix: '@@',
      basepath: dir.src + 'views/components'
    }))
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(gulp.dest(htmlConfig.build))
}
exports.html = html

/**************** javascript task ****************/
const jsConfig = {
  src: dir.src + 'assets/js/*.js',
  build: dir.build + 'assets/js/'
}

function javascript () {
  return gulp.src([
    'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    jsConfig.src
  ])
    .pipe(concat('main.js'))
    .pipe(terser())
    .pipe(gulp.dest(jsConfig.build))
}
exports.javascript = javascript

/**************** images task ****************/
const imgConfig = {
  src: dir.src + 'assets/images/**/*',
  build: dir.build + 'assets/images/',
  minOpts: {
    optimizationLevel: 5
  }
}

function images () {
  return gulp.src(imgConfig.src)
    .pipe(newer(imgConfig.build))
    .pipe(imagemin(imgConfig.minOpts))
    .pipe(size({
      showFiles: true
    }))
    .pipe(gulp.dest(imgConfig.build))
}
exports.images = images

/**************** CSS task ****************/
const cssConfig = {
  src: dir.src + 'assets/scss/main.scss',
  watch: dir.src + 'assets/scss/',
  build: dir.build + 'assets/css/',
  sassOpts: {
    imagePath: 'assets/images/',
    includePaths: [ 'node_modules/bootstrap/', 'src/assets/scss/partials' ],
    precision: 3,
    errLogToConsole: true
  },

  postCSS: [
    require('postcss-uncss')({
      html: [ 'build/*.html', 'build/admin/*.php' ],
      timeout: 100,
    }),
    require('postcss-assets')({
      loadPaths: [ 'assets/images/' ],
      basePath: dir.build
    }),
    require('autoprefixer')(),
    require('cssnano')
  ]
}

function postCSS () {
  return gulp.src(cssConfig.src)
    .pipe(sass(cssConfig.sassOpts).on('error', sass.logError))
    .pipe(postcss(cssConfig.postCSS))
    .pipe(size({
      showFiles: true
    }))
    .pipe(gulp.dest(cssConfig.build))
}
exports.postCSS = gulp.series(images, postCSS)

function css () {
  return gulp.src(cssConfig.src)
    .pipe(sass(cssConfig.sassOpts).on('error', sass.logError))
    .pipe(size({
      showFiles: true
    }))
    .pipe(gulp.dest(cssConfig.build))

}
exports.css = gulp.series(images, css)

/**************** watch task ****************/
function watch (done) {

  // image changes
  gulp.watch(imgConfig.src, images)

  // CSS changes
  gulp.watch(cssConfig.watch, css)

  // HTML changes
  gulp.watch(htmlConfig.paths, html)

  // javascript changes
  gulp.watch(jsConfig.src, javascript)

  // php changes
  gulp.watch(phpConfig.src, php)

  done()
}

// TinyMCE Icons
function tinyIcons () {
  return gulp.src([ 'node_modules/tinymce/icons/**/*' ]).pipe(gulp.dest(jsConfig.build + '/tinymce/icons/'))
}
exports.tinyIcons = tinyIcons

// TinyMCE Plugins
function tinyPlugins () {
  return gulp.src([ 'node_modules/tinymce/plugins/**/*' ]).pipe(gulp.dest(jsConfig.build + '/tinymce/plugins/'))
}
exports.tinyPlugins = tinyPlugins

// TinyMCE Skins
function tinySkins () {
  return gulp.src([ 'node_modules/tinymce/skins/**/*' ]).pipe(gulp.dest(jsConfig.build + '/tinymce/skins/'))
}
exports.tinySkins = tinySkins

// TinyMCE Themes
function tinyThemes () {
  return gulp.src([ 'node_modules/tinymce/themes/**/*' ]).pipe(gulp.dest(jsConfig.build + '/tinymce/themes/'))
}
exports.tinyThemes = tinyThemes

function tinyMain () {
  return gulp.src([
    'node_modules/tinymce/jquery.tinymce.min.js',
    'node_modules/tinymce/tinymce.js',

  ])
    .pipe(gulp.dest(jsConfig.build + '/tinymce'))
}
exports.tinyMain = tinyMain

exports.tinymce = gulp.series(tinyIcons, tinyPlugins, tinySkins, tinyThemes, tinyMain)


/**************** default task ****************/
exports.default = exports.dev = gulp.series(html, php, exports.css, exports.tinymce, javascript, watch)

/**************** production ******************/
exports.prod = gulp.series(html, php, exports.postCSS, javascript, exports.tinymce)
