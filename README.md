# Waraqa Publishing Group
This is a website for waraqa publishing group.
### Project Structure
```
.waraqa/
|   .gitignore
|   gulpfile.js
│   README.md
│   package.json    
│
|───build/
|   └───assets/
|       |───images/
|       |───css/
|       |───js/
|       └───*.html
|───src/
|   └───assets/
|       |───images/
|       |───scss/
|       |   |───partials/
|       |   |   └───_*.scss
|       |   └───main.scss
|       |───js/
|       └───views/
|           |───components/
|           └───*.html
```

### Task Runner
For Development, use vs code live serve extention and run it from ```build/```
```console
$ yarn start
or
$ gulp dev
```
For production
```console
$ yarn prod
or
$ gulp prod
```
This will remove all the unused css from production build and also minify html and css