<?php
  // Initialize the session
  session_start();
  if ( !isset( $_SESSION["loggedin"] ) || $_SESSION["loggedin"] !== true ) { header("location: login.php"); exit; }
  // Include config file
  require_once $_SERVER['DOCUMENT_ROOT']."/admin/config.php";
  require $_SERVER['DOCUMENT_ROOT'].'/admin/shared/header.php';


  $id = $_GET["id"];
  $sql = "SELECT * FROM policies WHERE id='$id'";
  $result = mysqli_query($link, $sql);
  $row = mysqli_fetch_assoc($result);

  $dis = $row['dis'];
  $name = $row['name'];
  $url = $row['link'];


  $dis_err = $shortDis_error = $name_err = "";

  if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {

    if(empty(trim($_POST["dis"]))){
      $dis_err = "Please enter some content.";
    } else {
      $dis =  mysqli_real_escape_string($link, trim($_POST["dis"]));
    }

    if(empty(trim($_POST["name"]))){
      $name_err = "Please enter some name.";
    } else {
      $name = mysqli_real_escape_string($link, trim($_POST["name"]));
    }
   


    if(empty($dis_err) && empty($name_err)) { 
      $qur = "UPDATE policies SET dis='$dis', name='$name' WHERE id='$id'";
     
      
      if (mysqli_query($link, $qur)) {
        $success = "Updated Successfully";
        $dis = trim($_POST["dis"]);
        $name = trim($_POST["name"]);

      } else {
        $error = "Error editing policy: " . mysqli_error($link);
        $dis = trim($_POST["dis"]);
        $name = trim($_POST["name"]);
      }

    }
    mysqli_close($link);
  }
?>

<div class="bg-secondary">
  <div class="container text-center py-5">
    <h3 class="text-primary font-weight-bold">Edit policy</h1>
    <p class="text-white lead"><?=$name;?></p>
  </div>
</div>

<div class="container mt-5">
  <?php
    if(isset($success)) {
      echo  '<h4 class="alert alert-success text-center alert-dismissible fade show">'. $success .' <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button> </h4>';
    } 
    if(isset($error)) {
      echo  '<h4 class="alert alert-danger text-center alert-dismissible fade show">'. $error .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button> </h4>';
    } 
  ?>
  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]). '?id='.$id; ?>" method="post">
  <div class="row">
    <div class="col-md-12 mt-4">
      <div class="form-group">
        <label for="name">Title </label>
        <input type="text" name="name" class="form-control" required value="<?=$name?>">
        <small class="help-block  text-left float-left text-danger"><?php echo $name_err; ?></small>
      </div>
    </div>
  
    <label class="mt-4 " for="dis">Policy</label>
    <div class="text-center mt-5 col-md-12 pt-5" id="spinner"> 
      <div class="spinner-border" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <div class="form-group col-md-12">
      <textarea name="dis" cols="30" rows="10" id="editor" class="editor d-none"><?= $dis ?></textarea>
      <small class="help-block text-left float-left text-danger"><?php echo $dis_err; ?></small>
    </div>
  </div>

  <button type="save" class="btn-primary btn mt-5 btn-lg btn-block">Save</button>
  </form>
</div>

<?php require $_SERVER['DOCUMENT_ROOT'].'/admin/shared/footer.php'; ?>
<?php require $_SERVER['DOCUMENT_ROOT'].'/admin/shared/editor.php'; ?>
