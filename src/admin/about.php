<?php
  // Initialize the session
  session_start();
  if ( !isset( $_SESSION["loggedin"] ) || $_SESSION["loggedin"] !== true ) { header("location: login.php"); exit; }
  // Include config file
  require_once "config.php";
  require 'shared/header.php';

  $sql = "SELECT id, content FROM about WHERE id=1";
  $result = mysqli_query($link, $sql);
  $row = mysqli_fetch_assoc($result);
  $content = $row["content"];
  
  $content_err = "";

  if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
    if(empty(trim($_POST["content"]))){
      $content_err = "Please enter some content.";
    } else {
      $data =  mysqli_real_escape_string($link, trim($_POST["content"]));
    }

    if(empty($content_err)) {
    $qur = "UPDATE about SET content='$data' WHERE id=1";
      if (mysqli_query($link, $qur)) {
        $success = "Updated Successfully";
        $content =trim($_POST["content"]);
        
      } else {
        $error = "Error updating record: " . mysqli_error($link);
        $content = trim($_POST["content"]);
      }
    }

    mysqli_close($link);
  }
?>

<div class="bg-secondary">
  <div class="container text-center py-5">
    <h3 class="text-primary font-weight-bold">About us</h1>
    <p class="text-white lead">Change the content of About us page</p>
  </div>
</div>

<div class="container mt-5">
  <?php
    if(isset($success)) {
      echo  '<h4 class="alert alert-success text-center alert-dismissible fade show">'. $success .' <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button> </h4>';
    }
    if(isset($error)) {
      echo  '<h4 class="alert alert-danger text-center alert-dismissible fade show">'. $error .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button> </h4>';
    }
  ?>
  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
  <div class="text-center mt-5 pt-5" id="spinner"> 
    <div class="spinner-border" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>
  <textarea name="content" id="" cols="30" rows="10" id="editor" class="editor d-none"><?= $content ?></textarea>
  <small class="help-block  text-left float-left text-danger"><?php echo $content_err; ?></small>

  <button type="save" class="btn-primary btn mt-5 btn-lg btn-block">Save</button>
  </form>
</div>

<?php require 'shared/footer.php'; ?>
<?php include 'shared/editor.php'; ?>
