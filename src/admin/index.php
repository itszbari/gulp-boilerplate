<?php
  // Initialize the session
  session_start();
  if ( !isset( $_SESSION["loggedin"] ) || $_SESSION["loggedin"] !== true ) { header("location: login.php"); exit; }
  require 'shared/header.php';
  
?>

<div class="bg-secondary">
  <div class="container py-5 text-center">
    <h3 class="text-primary font-weight-bold">Welcome</h3>
    <p class="text-white lead"><?php echo htmlspecialchars($_SESSION["username"]); ?></p>
  </div>
</div>


<?php require 'shared/footer.php'; ?>
