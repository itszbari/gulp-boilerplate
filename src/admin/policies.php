<?php
  // Initialize the session
  session_start();
  if ( !isset( $_SESSION["loggedin"] ) || $_SESSION["loggedin"] !== true ) { header("location: login.php"); exit; }
  
  // Include config file
  require_once "config.php";
  require 'shared/header.php';

  $sql = "SELECT * FROM policies";
  $result = mysqli_query($link, $sql);
  
  if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {

    $id = $_POST['id'];
    $del = "DELETE FROM policies WHERE id='$id'";

    if (mysqli_query($link, $del)) {
      $success = "Record deleted successfully";
      $sql = "SELECT * FROM policies";
      $result = mysqli_query($link, $sql);
    } else {
      $error = "Error deleting record: " . mysqli_error($conn);
    }
  }
  
?>

<div class="bg-secondary">
  <div class="container text-center py-5">
    <h3 class="text-primary font-weight-bold">Policies</h1>
    <p class="text-white lead">Add, Edit or Delete policies here</p>
  </div>
</div>
<div class="container">
  <?php
    if(isset($success)) {
      echo  '<h4 class="alert alert-success text-center alert-dismissible fade show">'. $success .' <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button> </h4>';
    } 
    if(isset($error)) {
      echo  '<h4 class="alert alert-danger text-center alert-dismissible fade show">'. $error .'<button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button> </h4>';
    } 
  ?>
</div>
<div class="container card p-md-5 mt-5">
  <?php 
    if (mysqli_num_rows($result) > 0) {
      // output data of each row
      echo "<a class='btn btn-primary mb-4 btn-sm btn-block' href='/admin/policies/add.php'>Add policies</a>";
    }
  ?>
<table class="table table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Link</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
    <?php

      if (mysqli_num_rows($result) > 0) {
        // output data of each row
        $x = 1;
        while($row = mysqli_fetch_assoc($result)) {
      ?>
        <tr>
          <th style="width: 40px" scope="row"><?= $x; $x++ ?></th>
          <td><?= $row['name']; ?></td>
          <td><?= $row['link']; ?></td>
          <td style="width: 80px"><a href="/admin/policies/edit.php?id=<?= $row['id']; ?>" class="btn btn-block btn-primary btn-sm">Edit</a></td>
          <td style="width: 80px">
            <a 
              href="#" 
              class="btn btn-block btn-danger btn-sm"
              onclick="event.preventDefault();document.getElementById('delete').submit();"
            >
              Delete
              <form id="delete" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" style="display: none;">
                <input type="hidden" name="id" value="<?=$row['id'];?>">
              </form>
            </a>
          </td>
        </tr>
    <?php
        }
      } else {
        echo "<tr>
                <td colspan='6' class='text-center'>No policies</td>
              </tr>
              <tr>
                <td colspan='6' class='text-center'><a class='btn btn-primary btn-sm btn-block' href='/admin/policies/add.php'>Add policy</a></td>
              </tr>";
      }
    ?>
    
  </tbody>
</table>
</div>


<?php require 'shared/footer.php'; ?>
<?php include 'shared/editor.php'; ?>
