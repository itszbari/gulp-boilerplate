<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link rel="stylesheet" href="/assets/css/main.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-secondary px-3 px-md-5">
  <a class="navbar-brand" href="/"><img src="/assets/images/logo.svg" height="40px" alt="" /></a>
  <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-filter-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" d="M14 10.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0 0 1h3a.5.5 0 0 0 .5-.5zm0-3a.5.5 0 0 0-.5-.5h-7a.5.5 0 0 0 0 1h7a.5.5 0 0 0 .5-.5zm0-3a.5.5 0 0 0-.5-.5h-11a.5.5 0 0 0 0 1h11a.5.5 0 0 0 .5-.5z"/>
    </svg>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/admin/index.php">Dashboard</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pages
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/admin/about.php">About us</a>
          <div class="dropdown-divider"></div>
          <div class="dropdown-header">News</div>

          <a class="dropdown-item" href="/admin/news.php">News & Events</a>
          <a class="dropdown-item" href="/admin/news/add.php">Add New News</a>
          <div class="dropdown-divider"></div>
          <div class="dropdown-header">Policies</div>
          <a class="dropdown-item" href="/admin/policies.php">Policies</a>
          <a class="dropdown-item" href="/admin/policies/add.php">Add New Policy</a>
         
        </div>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/admin/reset.php">Reset Password</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="/admin/logout.php">Logout</a>
      </li>

    </ul>
  </div>
</nav>