<script src="/assets/js/tinymce/tinymce.js"></script>
<script>
  tinymce.init({ 
    plugins: 'paste importcss autolink directionality link table charmap advlist lists wordcount charmap',
    menubar: false,
    toolbar: 'fontselect fontsizeselect formatselect | bold italic underline | alignleft aligncenter alignright | forecolor backcolor | numlist bullist | link | charmap table customTemplates',
    contextmenu: false,
    toolbar_sticky: true,
    importcss_append: true,
    height: 400,
    selector: 'textarea.editor',
    setup: (editor) => {
        editor.on('init', () => {
          //all your after init logics here.
          document.getElementById('spinner').style.display = "none";
          document.getElementById('editor').style.display = "block";
        })
      }
  });
</script>